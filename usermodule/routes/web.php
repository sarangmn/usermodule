<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[UserController::class,'index']);
Route::get('/new',[UserController::class,'create']);
Route::get('/edit/{id}',[UserController::class,'edit']);
Route::get('/view/{id}',[UserController::class,'show']);
Route::get('/delete/{id}',[UserController::class,'destroy']);
Route::post('/new/create/',[UserController::class,'store']);
Route::post('/edit/update/{id}',[UserController::class,'update']);

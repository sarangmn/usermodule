<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Useradd;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index')->with('user_arr',Useradd::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'=>['required','min:3','max:100'],
            'email'=>'required',
            'mobile'=>'required',
            'place'=>'required',
        ]);

        $member=new Useradd;
        $member->name = $request -> input('name');
        $member->email = $request -> input('email');
        $member->mobile = $request -> input('mobile');
        $member->place = $request -> input('place');
        $member->save();
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $task=Useradd::find($id);
        
        return view('user.showone')->with('data',$task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $task=Useradd::find($id);
        return view('user.edit')->with('data',$task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Useradd $useradds, $id)
    {
        request()->validate([
            'name'=>['required','min:3','max:100'],
            'email'=>'required',
            'mobile'=>'required',
            'place'=>'required',
        ]);
        $task=Useradd::where('id',$request->id)->first();
        var_dump($task);
        $task->name=$request->input('name');
        $task->email=$request->input('email');
        $task->mobile=$request->input('mobile');
        $task->place=$request->input('place');
        $task->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Useradd $useradds,$id)
    {
        Useradd::destroy($id);
        return redirect('/');
    }
}

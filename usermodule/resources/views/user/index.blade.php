<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UserModule</title>
</head>
<body>
    <div>
    <h1>Users List</h1>
    <div>
    <br><br>

    
    <table>
    <tr>
    <td>sl.no</td>   
    <td>NAME</td>
    <td>EMAIL</td> 
    <td>MOBILE</td> 
    <td>PLACE</td> 
    <td>ACTION</td> 
    </tr>
    @foreach($user_arr as $users)
    
    <li>
    <tr>
    <td>{{$users->id}}</td>
    <td><a href="\view\{{$users->id}}" style="text-decoration:none;">{{$users->name}}</a></td>
    <td>{{$users->email}}</td>
    <td>{{$users->mobile}}</td>
    <td>{{$users->place}}</td>
    <td>
    <a href="\edit\{{$users->id}}">edit</a><br>
    </td>
    </tr>
    </li>
    
    @endforeach
    </table>
    

    
    <br><br>
    <a href="\new">Add New User</a>
</body>
</html>
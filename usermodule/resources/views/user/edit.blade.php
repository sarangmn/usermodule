<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>create-user</title>
</head>
<body>
    <a href="\">back</a><br>
    <h1>edit details of user </h1>
    <br><br>
    <form action="/edit/update/{{$data->id}}" method="POST">
    {{ csrf_field() }}
    <div>
<input type="text" value="{{$data->name}}" name="name"><br>
@error('name')
    <h5 style="color:red;">{{$errors->first("name")}}</h5>
@enderror
<br>
<input type="email" value="{{$data->email}}" name="email"><br>
@error('email')
    <h5 style="color:red;">{{$errors->first("email")}}</h5>
@enderror
<br>
<input type="tel" value="{{$data->mobile}}" name="mobile"><br>
@error('mobile')
    <h5 style="color:red;">{{$errors->first("mobile")}}</h5>
@enderror
<br>
<input type="text" value="{{$data->place}}" name="place"><br>
@error('place')
    <h5 style="color:red;">{{$errors->first("place")}}</h5>
@enderror
<br>
<button type="submit">update user</button>
</div>
</form>

    
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>create-user</title>
</head>
<body>
    <h1>enter details of new user<h1>
    <br><br>
    <form action="/new/create" method="POST">
    {{ csrf_field() }}
    <div>
<input type="text" placeholder="enter name" name="name" value="{{old('name')}}"><br>

@error('name')
    <h5 style="color:red;">{{$errors->first("name")}}</h5>
@enderror


<input type="email" placeholder="enter email" name="email" value="{{old('email')}}"><br>

@error('email')
    <h5 style="color:red">{{$errors->first("email")}}</h5>
@enderror

<input type="tel" placeholder="enter mobile" name="mobile" value="{{old('mobile')}}"><br>

@error('mobile')
    <h5 style="color:red">{{$errors->first("mobile")}}</h5>
@enderror

<input type="text" placeholder="enter place" name="place" value="{{old('place')}}"><br>

@error('place')
    <h5 style="color:red">{{$errors->first("place")}}</h5>
@enderror

<button type="submit">create user</button>
</div>
</form>
    
</body>
</html>